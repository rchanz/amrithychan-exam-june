import React from "react";
import { Switch, Route } from "react-router-dom";
import "./App.css";
import PostList from "./components/PostList";
import Post from "./components/Post";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/">
          <PostList />
        </Route>
        <Route exact path="/posts/:id">
          <Post />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
