import axios from "axios";
import { GET_POSTS, GET_POST_BY_ID } from "./postActionTypes";

export const getPosts = () => {
  return async (dp) => {
    const result = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );
    dp({
      type: GET_POSTS,
      data: result.data,
    });
  };
};

export const getPostByID = (id) => {
  return async (dp) => {
    const result = await axios.get(
      `https://jsonplaceholder.typicode.com/posts/${id}`
    );
    dp({
      type: GET_POST_BY_ID,
      data: result.data,
    });
  };
};
