import { GET_POSTS, GET_POST_BY_ID } from "../actions/postActionTypes";

const defaultState = {
  data: [],
  post: null,
};

export const postReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return {
        ...state,
        data: action.data,
      };
    case GET_POST_BY_ID:
      return {
        ...state,
        post: action.data,
      };
    default:
      return state;
  }
};
