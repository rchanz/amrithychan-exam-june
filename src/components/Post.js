import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getPostByID } from "../redux/actions/postActions";
import { withRouter } from "react-router-dom";

class Post extends Component {
  componentDidMount() {
    const {
      match: { params },
    } = this.props;
    this.props.getPostByID(params.id);
  }

  render() {
    if (this.props.post) {
      return (
        <div className="PostWrapper">
          <h1>{this.props.post.title}</h1>
          <p>{this.props.post.body}</p>
        </div>
      );
    }
    return <div></div>;
  }
}

const mapStateToProps = (state) => {
  return {
    post: state.post,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getPostByID,
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Post));
