import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getPosts, getPostByID } from "../redux/actions/postActions";
import { Link } from "react-router-dom";

function PostList(props) {
  useEffect(() => {
    props.getPosts();
  }, []);
  return (
    <div>
      {props.data.map((post) => {
        return (
          <div key={post.id}>
            Tutorial {post.id}
            {" : "}
            <Link to={`/posts/${post.id}`}>{post.title}</Link>
          </div>
        );
      })}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    data: state.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getPosts,
      getPostByID,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(PostList);
